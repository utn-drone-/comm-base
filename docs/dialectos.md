# Dialectos MAVLink

La definición de los distíntos dialectos de MAVLink se realiza en archivos .xml dentro del directorio “message_definitions” y cada dialecto tiene su propio archivo de definición. Al querer definir un dialecto nuevo, se crea en dicha ubicación un nuevo archivo con la forma “proyecto.xml”.

## Estructura de un dialecto
### Estructura básica
``` xml  
<?xml version="1.0">
<mavlink>
	<include>archivo_a_incluir.xml</include>
	...
	<version></version>
	<enums>
		Definición de enumeraciones.
	</enums>
	<messages>
		Definición de mensajes.
	</messages>
</mavlink>  
```
### Inclusión de archivos
Al incluir un archivo con la cláusula:
``` xml
<include>archivo_a_incluir.xml</include>
```
automáticamente todos las enumeraciones y los mensajes definidos en dicho archivo formarán parte del dialecto que se está definiendo. Generalmente se incluirá el archivo "common.xml".

### Definición de una enumeración
Las enumeraciones se definen de la siguiente manera:
``` xml
<enum name="NOMBRE_DE_LA_ENUMERACIÓN">
	<description>Descripción general de la enumeración</description>
	<entry value="VALOR" name="NOMBRE_DE_LA_ENTRADA">
		<description>Descripción de la entrada</description>
		<param index="INDICE">Descripción del parámetro</param>
		...
	</entry>
	...
</enum>
```
Los valores de las entradas son siempre números enteros.
En cuanto a los nombres de las enumeraciones y las entradas:
* Por convención se escriben en letras mayúsculas y con las palabras separadas por guiones bajos.
* Los nombres de las entradas se forman con el nombre de la enumeración a la que pertenecen y aquello particular que representan. Por ejemplo; dentro de la enumeración  "MAV_AUTOPILOT", una de las entradas posibles es "MAV_AUTOPILOT_GENERIC".

### Definición de un mensaje
Los mensajes se definen de la siguiente manera:
``` xml
<message id="ID_DEL_MENSAJE" name="NOMBRE_DEL_MENSAJE">
	<description>Descripción del mensaje</description>
	<field type="TIPO_DE_DATO" name="NOMBRE_DEL_CAMPO">Descripción del campo</field>
	...
</message>
```
Para la definición de dialectos propios, se encuentran reservados los ids de mensajes del 180 al 229.

#### Tipos de dato
* Enteros:
	* Con signo: se escriben en el archivo .xml con la forma "intX_t", donde X es la cantidad de bits que conforman el número. Los valores de X posibles de X son 8, 16, 32 y 64 bits (1, 2, 4 y 8 bytes respectivamente).
	* Sin signo: se escriben de la forma "uintX_t". Nuevamente, X es la cantidad de bits que conforman el número y sus valores posibles son 8, 16, 32 y 64.
* Punto flotante: se escriben de la forma "float" y utilizan el formato IEEE 754 de 32 bits.
