from pymavlink import mavutil

#Se debe setear el dialecto antes de crear la conexion al que se quiera utilizar.
#Por defecto parece utilizar Ardupilotmega.
#Debe setearse el dialecto antes de crear alguna conexion.
mavutil.set_dialect('utn_drone')

connection = mavutil.mavlink_connection('tcpin:127.0.0.1:15001')

while True:
	#recv_msg decodifica el mensaje, no hace falta hacerlo a mano.
	#recv_match(type='TIPO_DE_MENSAJE') espera a recibir dicho tipo
	msg = connection.recv_msg()
	if msg is not None:
		print(msg.get_type())
		print(msg)
