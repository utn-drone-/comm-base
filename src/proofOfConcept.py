from pymavlink import mavutil
import sys
import thread

def loopGC(groundControl,drone):
    while(1):
        msg = groundControl.recv()
        if msg is not None:
            drone.write(msg)
        pass


def main(argv):
    puertoDrone = argv[0]
    puertoGroundControl = argv[1]

    print ("Incoming drone messages on port {}...".format(puertoDrone))
    print ("Incoming ground control connections on port {}...".format(puertoGroundControl))

    drone = mavutil.mavlink_connection("tcpin:0.0.0.0:{}".format(puertoDrone))
    groundControl = mavutil.mavlink_connection("tcpin:0.0.0.0:{}".format(puertoGroundControl))

    thread.start_new_thread(loopGC,(groundControl,drone,))

    raw_input("Press Enter to continue...")

    while(1):
        msg = drone.recv()
        if msg is not None:
            print msg
            groundControl.write(msg)

if __name__ == "__main__":
    main(sys.argv[1:])
