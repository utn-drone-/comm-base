from pymavlink import mavutil
from pymavlink.dialects.v10.utn_drone import *

mavutil.set_dialect("utn_drone")

connection = mavutil.mavlink_connection('tcp:127.0.0.1:15001')

while True:
	print("Sending test message")
	# el campo mav de la conexion es un objeto MAVLink que corresponde al dialecto
	# con el que se ha configurado mavutils. Los objetos MAVLink son los encargados de
	# generar los mensajes.
	connection.mav.test_message_send(MAV_TYPE_GENERIC, 15, MAV_MODE_AUTO_DISARMED, 0, MAV_STATE_STANDBY)
