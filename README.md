# Base de comunicaciones

Software de interconexion de drones y bases de control basado en MAVLink (Implementacion en python: pymavlink)

## Requerimientos
* Lenguaje: Python 2
* Gestror de paquetes: pip (Python 2)

## Dependencias

Para poder correr pymavlink hacen falta las siguentes dependencias:

### Paquetes de linux

Son necesarios los siguientes paquetes para poder utilizar el paquete  `lxml` (ver [Paquetes de Python](#dep-python))


- libxml2-dev
- libxslt-dev
- python-dev

### <a name="dep-python"></a>Paquetes de Python

- future : interoperabilidad entre python 2 y 3 (http://python-future.org/)
- lxml : para parsear ficheros xml (http://lxml.de/installation.html)

## Instalacion

1. Clonar repositorio
  Tener en cuenta que es necesario bajar el submodulo pymavlink para poder funcionar, para ello podemos:

  _Usar el flag `--recursive` al clonar el repositorio_:

  `git clone https://gitlab.com/utn-drone-/comm-base --recursive`

  _O si el repositorio ya fue clonado podemos correr los siguientes comandos para bajar el submodulo:_

  ```
    git submodule init
    git submodule update
  ```



2. Instalar paquetes de linux

  _Para Ubuntu/Debian_

  `sudo apt install libxml2-dev libxslt-dev python-dev`

  _O el comando de gestor de paquetes de la distribucion en uso_

3. Instalar paquetes de Python

  `sudo pip2 install -U future lxml`

  _El flag -U permite actualizar future y lxml en el caso de que estuvieran ya instalados._

4. Correr setup.py
  En el directorio pymavlink se encuentra un archivo `setup.py`, el cual debemos correr con el argumento `install`

  `sudo python pymavlink/setup.py install`

Distributed as-is; no warranty is given.

* Your friends at UTN.
